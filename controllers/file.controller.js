const path = require("path");
const multer = require("multer");
const { v4: uuidv4 } = require('uuid');

const fileService = require("../services/file.service");

const { existsFolder, moveFile, deleteFile } = require("../utils/fileManagement.utils");
const { checkFileType } = require("../utils/upload.utils");

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const pathTemp = "./public/files/tmp";
    existsFolder(pathTemp);
    cb(null, pathTemp);
  },
  filename: (req, file, cb) => {
    cb(null, `${Date.now()}_${file.originalname}`);
  },
});

const fileMulter = multer({
  storage: storage,
  limits: { fileSize: 60 * 1024 * 1024, fieldSize: 60 * 1024 * 1024 }, // 30mb
  fileFilter: (req, file, cb) => {
    checkFileType(req, file, cb);
  },
});

const upload = fileMulter.single("file");

const file = async (req, res, next) => {
  try {
    res.status(200).json({ message: "Upload Success", file: { type: req.body.type, movePath: req.body.movePath, ...req.file } });
  } catch (error) {
    next(error);
  }
};

const fileBulk = async (req, res) => {
  res.status(200).json({ text: "fileBulk" });
};

const fileMove = async (req, res, next) => {
  try {
    const listFiles = req.body?.files || [];
    const fileList = fileService.get();
    const newFileList = [...fileList];

    listFiles.map((list, key) => {
      newFileList.push({
        id: uuidv4(),
        path: list.movePath,
        type: list.type,
        fileType: path.extname(list.filename),
        size: list.size,
        name: list.filename,
        objTypeOf: list.objTypeOf,
        refObj: list.refObj,
      });
      moveFile(list.oldPath, list.newPath);
    });
    fileService.update(newFileList);
    res.status(200).json({ message: "Move Success" });
  } catch (error) {
    next(error);
  }
};

const fileDelete = async (req, res, next) => {
  try {
    const listFiles = req.body?.files || [];
    console.log(listFiles);
    let newFileList = fileService.get();
    listFiles.map((list, key) => {
      deleteFile(list.path);
      newFileList = newFileList.filter((x) => `${x?.path}/${x?.name}` !== list.path);
    });
    fileService.update(newFileList);
    res.status(200).json({ message: "Delete Success" });
  } catch (error) {
    next(error);
  }
};

module.exports = {
  upload,
  file,
  fileBulk,
  fileMove,
  fileDelete,
};
