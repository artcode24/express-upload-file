const express = require("express");

const fileController = require("../controllers/file.controller");

const router = express.Router();

router.post("/upload", fileController.upload, fileController.file);
// router.post("/bulk", fileController.fileBulk);
router.post("/move", fileController.fileMove);
router.post("/delete", fileController.fileDelete);

module.exports = router;
