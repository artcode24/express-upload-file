const { readFile, rewriteFile } = require("../utils/fileManagement.utils");

const get = () => {
  const fileList = readFile("./database/fileList.json");
  return JSON.parse(fileList);
};

const update = (newFileList) => {
  rewriteFile("./database/fileList.json", JSON.stringify(newFileList));
};

module.exports = {
  get,
  update,
};
