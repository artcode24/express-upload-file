const whiteList = {
  media: [
    "image/png", // .png
    "image/jpeg", // .jpeg || .jpg
  ],
  document: [
    "application/pdf", // .pdf
    "application/msword", // .doc
    "application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .docx
    "application/vnd.ms-excel", // .xls
    "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xlsx
  ],
};

module.exports = {
  whiteList,
};
