const { whiteList } = require("./constants");
const checkFileType = (req, file, cb) => {
  for (const key of Object.keys(whiteList)) {
    const isExist = whiteList[key].indexOf(file.mimetype);
    if (isExist !== -1) {
      req.body.type = key;
      req.body.movePath = `./public/files/${key}`;
      break;
    }
  }
  if (req.body?.type) {
    cb(null, true);
  } else {
    cb("Error: Do Not Support Type File, Can't Upload File");
  }
};

module.exports = {
  checkFileType,
};
