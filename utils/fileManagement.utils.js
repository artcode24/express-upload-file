const fs = require("fs");

const createFolder = (path) => {
  try {
    fs.mkdirSync(path, { recursive: true });
    fs.chmodSync(path, 777);
  } catch (error) {
    throw new Error(error);
  }
}

const existsFolder = (path) => {
  try {
    if (!fs.existsSync(path.split("/").slice(0, -1).join("/"))){
      createFolder(path.split("/").slice(0, -1).join("/"));
    }
  } catch (error) {
    throw new Error(error);
  }
}

const createFile = (path, message) => {
  try {
    existsFolder(path);
    const stream = fs.createWriteStream(path);
    stream.once("open", (fd) => {
      stream.write(message);
      stream.end();
    });
  } catch (error) {
    throw new Error(error);
  }
};

const rewriteFile = (path, message) => {
  try {
    fs.writeFileSync(path, message);
    console.log("The file was saved!");
  } catch (error) {
    throw new Error(error);
  }
};

const writeFile = (path, message) => {
  try {
    fs.appendFileSync(path, message);
    console.log("successfully the message has been added to the file");
  } catch (error) {
    throw new Error(error);
  }
};

const deleteFile = (path) => {
  try {
    fs.unlinkSync(path);
    console.log("successfully deleted the file");
  } catch (error) {
    throw error;
  }
};

const renameFile = (path, oldFileName, newFileName) => {
  try {
    fs.renameSync(path + oldFileName, path + newFileName);
    console.log("successfully renamed the file");
  } catch (error) {
    throw new Error(error);
  }
};

const readFile = (path) => {
  try {
    const content = fs.readFileSync(path, "UTF-8");
    return content;
  } catch (error) {
    throw new Error(error);
  }
};

const copyFile = (oldPath, newPath) => {
  try {
    const content = fs.readFileSync(oldPath, "UTF-8");
    createFile(newPath, content);
  } catch (error) {
    throw new Error(error);
  }
};

const moveFile = (oldPath, newPath) => {
  try {
    existsFolder(newPath);
    fs.copyFileSync(oldPath, newPath);
    deleteFile(oldPath);
  } catch (error) {
    throw new Error(error);
  }
};

module.exports = {
  existsFolder,
  createFolder,
  createFile,
  rewriteFile,
  writeFile,
  deleteFile,
  renameFile,
  readFile,
  copyFile,
  moveFile,
};
